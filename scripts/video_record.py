#!/usr/bin/env python3
import time
import rospy
import signal
import argparse
from sensor_msgs.msg import Image, CompressedImage

import cv_bridge # CvBridge, CvBridgeError
import cv2

class VideoRecorder:
    def __init__(self, args):
        self.__cv_bridge = cv_bridge.CvBridge()
        self.__image = None
        self.__time = None
        rospy.Subscriber(
            args.topic,
            CompressedImage,
            self.__imageCallback
        )
        temp_res = args.resolution.split('x')
        self.__resolution = (int(temp_res[0]), int(temp_res[1]))
        self.__fps = int(args.fps)
        self.__T = 1./self.__fps
        self.__recorder = cv2.VideoWriter(
            args.output_file,
            cv2.VideoWriter_fourcc('m', 'p', '4', 'v'), 
            self.__fps, 
            self.__resolution
        )
        # self.__rate = rospy.Rate(self.__fps)
        signal.signal(signal.SIGINT, self.release)
        signal.signal(signal.SIGTERM, self.release)
        
    def release(self, sig, frame):
        self.__recorder.release()
        print("Video release...")
        exit(0)
    
    def __imageCallback(self, msg):
        try:
            if self.__time is None:
                self.__time = msg.header.stamp.to_sec()
            else:
                self.updateVideo(msg.header.stamp.to_sec())
            
            # cv_image = global_cv_bridge.compressed_imgmsg_to_cv2(rgb_raw, "passthrough")
            self.__image = self.__cv_bridge.compressed_imgmsg_to_cv2(msg, "passthrough")
            
        except cv_bridge.CvBridgeError as e:
            print(e)
        

    def updateVideo(self,to_time):
        while self.__time < to_time:
            self.__recorder.write(self.__image) 
            self.__time += self.__T
        
    

if __name__ == '__main__':

    try:
        ## Parse params
        parser = argparse.ArgumentParser()

        parser.add_argument('-o', '--output_file', type = str, default = "video.mp4", help = "" )
        parser.add_argument('-t', '--topic', type = str, default = "/usb_cam/image_raw/compressed", help = "Image topic name" )
        parser.add_argument('-f', '--fps', type = int, default = 30, help = "FPS of recorded video")
        parser.add_argument('-r', '--resolution', type = str,  default = "640x480", help = "Video resolution like: 640x480")

        args = parser.parse_args()

        rospy.init_node('video_recorder')
        vr = VideoRecorder(args)

        rospy.spin()
    except rospy.ROSInterruptException:
        vr.release()
