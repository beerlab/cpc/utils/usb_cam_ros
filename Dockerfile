ARG BASE_IMG

FROM ${BASE_IMG}

SHELL ["/bin/bash", "-ci"]
ENV DEBIAN_FRONTEND noninteractive

WORKDIR /workspace/ros_ws

RUN apt update && apt install -y \
    ros-noetic-camera-info-manager\
    ros-noetic-usb-cam && \
    rm -rf /var/lib/apt/lists/*

RUN python3 -m pip install opencv-contrib-python
COPY ./ /workspace/ros_ws/src/usb_cam_ros
RUN catkin build

CMD roslaunch usb_cam_ros usb_cam.launch