cmake_minimum_required(VERSION 3.2)
project(usb_cam_ros)

find_package(catkin REQUIRED COMPONENTS
    cv_bridge
    image_transport
    roscpp
    rospy
    std_msgs
    std_srvs
    sensor_msgs
    camera_info_manager
)

install(DIRECTORY launch/
    DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}/launch
    FILES_MATCHING PATTERN "*.launch"
)

catkin_package()
